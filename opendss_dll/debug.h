#pragma once

// debug toggle
//#define DEBUG_BUILD

#ifdef DEBUG_BUILD
	#include <iostream>
	#define debug_msg(x) (std::cout << (x) << std::endl)
	extern bool console_ok;  //defined in debug.cpp
# else
	#define debug_msg(x)  //no-ops all the debug messages
#endif
