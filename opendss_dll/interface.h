#pragma once
#define DllExport __declspec(dllexport)
#include "types.h"
#include <Windows.h>

extern "C" {

	DllExport int  __stdcall New(pTGeneratorVars Genvars, pTDynamicsRec DynaData, pTDSSCallBacks Callbacks);

	DllExport void __stdcall Delete(int32_t * ID);

	DllExport void __stdcall Select(int32_t * ID);

	DllExport void __stdcall Init(pComplexArray V, pComplexArray I);

	DllExport void __stdcall Calc(pComplexArray V, pComplexArray I);

	DllExport void __stdcall Integrate();

	DllExport void __stdcall Edit(pAnsiChar EditStr, Cardinal Maxlen);

	DllExport void __stdcall UpdateModel();

	DllExport void __stdcall Save();

	DllExport void __stdcall Restore();

	DllExport int  __stdcall NumVars();

	DllExport void __stdcall GetAllVars(double * Vars);

	DllExport double  __stdcall GetVariable(int32_t * i);

	DllExport void __stdcall SetVariable(int32_t * i, double * value);

	DllExport void __stdcall GetVarName(int32_t * VarNum, pAnsiChar VarName, uint32_t maxlen);

}