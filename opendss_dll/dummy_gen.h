#pragma once
#include "custom_gen.h"

class DummyGen : public CustomGen {
public:
	void initialize(TGeneratorVars * Genvars, TDynamicsRec * DynaData, TDSSCallBacks * Callbacks) override;
	void calc_cg(pComplexArray v, pComplexArray I) override;
	void edit(std::string edit_string) override;
	void update() override;
};