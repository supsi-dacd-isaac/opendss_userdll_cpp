#pragma once
#include "types.h"
#include <boost\container\map.hpp>

/* Abstract base class for generators with built-in parameter storage and manipulation
   and virtual stub methods*/

class CustomGen {

protected:
	// raw storage of variable names and value
	TDSSCallBacks * my_callbacks;
	boost::container::map<int, std::string>		var_names;
	boost::container::map<int, double>			var_values;

public:

	// pure virtuals
	virtual void		initialize(TGeneratorVars * Genvars, TDynamicsRec * DynaData, TDSSCallBacks * Callbacks) = 0;
	virtual void		calc_cg(ComplexArray * v, ComplexArray * I) = 0;
	virtual void		edit(std::string edit_string) = 0;
	virtual void		update() = 0;

	// built-in parameter processing
	int					get_n_vars();
	std::string			get_pname(int p_index);
	double				get_pvalue(int p_index);
	void				set_pvalue(int p_index, double value);
};
