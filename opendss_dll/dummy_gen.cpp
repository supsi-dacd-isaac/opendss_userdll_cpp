#include "stdafx.h"
#include "dummy_gen.h"
#include "debug.h"
#include <boost/algorithm/string/predicate.hpp>
#include <complex>

void DummyGen::initialize(TGeneratorVars * Genvars, TDynamicsRec * DynaData, TDSSCallBacks * Callbacks) {
	// initializing variable names
	this->my_callbacks = Callbacks;
	
	this->var_names[1] = "r";
	this->var_names[2] = "x";
	this->var_names[3] = "phases";

	this->var_values[0] = 1.0;
	this->var_values[1] = 1.0;
	this->var_values[2] = 3;

};

void DummyGen::calc_cg(pComplexArray v, pComplexArray I) {

	for (int i = 0; i < 3; i++)
	{
		I->_x[i] = Complex{ 0.0 , 0.0 };
	}
};

void DummyGen::edit(std::string edit_string) {

	/* 

	char resstr[255] = "get number";
	this->my_callbacks->DoDSSCommand(&resstr[0], 255);

	char resstr2[255] = "empty";  //gets overwritten
	this->my_callbacks->GetResultStr(&resstr2[0], 255);

	std::string s = std::string(resstr);
	std::string s2 = std::string(resstr2);

	*/

}

void DummyGen::update() {
	debug_msg("update triggered");
}