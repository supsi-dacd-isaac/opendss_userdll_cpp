#include "stdafx.h"
#include "interface.h"
#include <stdlib.h>
#include "debug.h"
#include <boost\container\map.hpp>


/* HERE YOU CHOOSE WHAT CLASS IS TO BE ACTUALLY USED*/
#include "dummy_gen.h"
#define GEN_CLASS DummyGen

#define MAX_INSTANCES 100000
unsigned int selected_cg_instance = 0;
boost::container::map<int, GEN_CLASS *> custom_gen_instances;


// my auxiliary functions
int find_new_instance_no() {
	for (int i = 1; i <= MAX_INSTANCES; i++) {
		if (!custom_gen_instances.count(i)) {
			return i;
		};
	};
	throw("The maximum number of custom generator instances was reached...something is probably wrong");
};

void notimpl() {
	throw("Dynamic mode not implemented!");
}


// begin interface implementation
int __stdcall New(pTGeneratorVars Genvars, pTDynamicsRec DynaData, pTDSSCallBacks Callbacks) {
	int new_cg_instance_no = find_new_instance_no();
	custom_gen_instances[new_cg_instance_no] = new GEN_CLASS;  // gets destroyed in "Delete" function
	custom_gen_instances[new_cg_instance_no]->initialize(Genvars, DynaData, Callbacks);
	return new_cg_instance_no;
};

void __stdcall Delete(int32_t * ID) {
	delete custom_gen_instances[*ID];  // uses the stored pointer to delete the object on the heap
	custom_gen_instances.erase(*ID);  // erases the pair from the storage map
};

void __stdcall Select(int32_t * ID) {
	selected_cg_instance = *ID;
};

void __stdcall Init(ComplexArray * V, ComplexArray * I) {
	notimpl();
};

void __stdcall Calc(ComplexArray * V, ComplexArray * I) {
	custom_gen_instances[selected_cg_instance]->calc_cg(V, I);
};

void __stdcall Integrate() {
	notimpl();
};

void __stdcall Edit(pAnsiChar EditStr, Cardinal Maxlen) {
	std::string es = std::string(EditStr, Maxlen);
	custom_gen_instances[selected_cg_instance]->edit(es);
};

void __stdcall UpdateModel() {
	custom_gen_instances[selected_cg_instance]->update();
};

void __stdcall Save() {
	// not used as of 2012
};

void __stdcall Restore() {
	// not used as of 2012
};
 
int  __stdcall NumVars() {
	return custom_gen_instances[selected_cg_instance]->get_n_vars();
};

double  __stdcall GetVariable(int32_t * i) {
	return custom_gen_instances[selected_cg_instance]->get_pvalue(*i);
};

void __stdcall GetAllVars(double * Vars) {
	for (int i = 0; i < NumVars(); i++) {
		Vars[i] = GetVariable(&i);
	};
};

void __stdcall SetVariable(int32_t *i, double *value) {
	custom_gen_instances[selected_cg_instance]->set_pvalue(*i, *value);
};

void __stdcall GetVarName(int32_t * VarNum, pAnsiChar VarName, uint32_t maxlen) {
	snprintf(VarName, maxlen, custom_gen_instances[selected_cg_instance]->get_pname(*VarNum).c_str());
};