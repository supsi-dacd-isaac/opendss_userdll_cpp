#include "stdafx.h"
#include "types.h"
#include "custom_gen.h"


/* built in parameter manipulation*/
std::string CustomGen::get_pname(int p_index) {
	return this->var_names[p_index];
};

double CustomGen::get_pvalue(int p_index) {
	return this->var_values[p_index];
};

void CustomGen::set_pvalue(int p_index, double value) {
	this->var_values[p_index] = value;
};

int CustomGen::get_n_vars() {
	return (int)this->var_names.size();
};